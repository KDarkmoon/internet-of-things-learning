
### 1、安装termux

```
apt-get update

apt-get install proot
```

### 2、安装Node-NED

```
apt install coreutils nano nodejs

npm i -g --unsafe-perm node-red

```
设置后台自动启动

```
echo "node-red &">>~/.bashrc
```

浏览器输入：[http://localhost:1880](http://localhost:1880)便可以访问node-red开发界面

### 3、安装mosquito
```
apt install mosquitto
```
### 4、配置SSH

```
apt install openssh
```

终端输入："sshd &" 便可以通过ssh访问termux终端

### 5、内网穿透

内网穿透采用cpolar

#### 5.1安装dns服务

打开termux

终端输入命令：

```
apt install dnsutils
```

(会创建一个DNS解析文件：$PREFIX/etc/resolv.conf,默认已添加了8.8.8.8)

#### 5.2下载cpolar(ARM版本)

终端输入命令：

```
curl -O -L https://www.cpolar.com/static/downloads/cpolar-stable-linux-arm.zip
```

#### 5.3解压缩

终端输入命令：

```
unzip cpolar-stable-linux-arm.zip
```

#### 5.4认证token

访问[cpolar官网](https://www.cpolar.com)注册cpolar账号，登录cpolar点击->仪表盘->验证复制自己的token值；

粘贴自己的token

```
./cpolar authtoken ×××××××××××××××
```

#### 5.5内网穿透：

单个端口穿透

./cpolar + 通信协议 + 端口(如:`./cpolar tcp 8022` 实现远程ssh访问termux)

多个端口批量穿透编辑cpolar.yml文件

```
vi .cpolar/cpolar.yml
```

文件中的配置信息如下：

```
authtoken: ××××××××××××××××××××××   #(你的token，认证token时自动添加的)
tunnels:
    node-red:                       #隧道名称，可自行定义，但多个隧道时不可重复
        addr:       1880            #本地站点端口
        proto:      tcp             #协议TCP
        region:     cn              #地区,cn_vip,可选:us,hk,cn,cn_vip
    ssh:
        addr:       8022
        proto:      tcp
        region:     cn
    mosquitto:
        addr:       1883
        proto:      tcp
        region:     cn

```

可以根据模板添加自己的的端口

该文件配置了node-red、ssh、mosquitto，保存后运行：`./cpolar start-all` 即可实现三个服务的远程访问